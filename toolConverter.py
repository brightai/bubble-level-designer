import sys
import math
import json

print( "Parsing file : " + sys.argv[1] )

f = open( sys.argv[1], 'r' );

jsonData = json.load( f )


colors = [ "blue", "green", "red", "yellow", "purple" ]
specials = [ "puffer", "clearer", "extra", "anchor", "blocker", "ink", "inkblocker", "flounder", "stone", "whirl", "starfish", "clear" ]

creatures = [ "save1", "save3", "save7", "boris" ]

def enum(**enums):
	return type( 'Enum', (), enums )

LevelType = enum(
	INVALID = -1,
	CLEARTOP = 0,
	ROTATING = 1,
	WATER = 2,
	CREATURES = 3,
	BORIS_SINGLE = 4,
	BORIS_MULTI = 5,
	)

def getQRFromXY( x, y, rowLength ):
	q = 0
	r = 0

	if levelType != LevelType.ROTATING and levelType != LevelType.BORIS_SINGLE:
		r = maxY - (y - minY)
		if (y&1) != (r&1):
			r += 1
		q = (x-minX) - math.floor( r / 2 ) - math.floor( rowLength / 2 )
	else:
		r = anchorY - y
		q = (x - anchorX) - math.floor( r / 2 )
		if (anchorY&1) == 1:
			q -= 1
			if (anchorY&1) == (r&1):
				q -= 1
	return (q,r)


pathFile = open( 'LevelDataPath.txt', 'r' )
rootFolder = pathFile.read();

manifestFile = open( rootFolder + "LevelManifest.txt", "w" )
mainfestObj = []

creatureSymbol = "CREATURES#";


for item in jsonData:
	for k,v in item.iteritems():
		outfilename = rootFolder + k + ".tdf"
		outfile = open( outfilename, 'w' )

		levelType = LevelType.INVALID
		
		minX = 999
		maxX = 0

		minY = 999
		maxY = 0

		anchorX = 0
		anchorY = 0

		outArray = []

		if v.find( creatureSymbol, ) != -1:
			creatureEnd = v.find( '#', len( creatureSymbol ) )
			gridArray = v[creatureEnd+2:].split( ';' )
			levelType = LevelType.CREATURES
		else:
			gridArray = v.split( ';' )

		for i in range( 0, len( gridArray ), 3 ):
			color = gridArray[i+2]

			if levelType != LevelType.CREATURES:
				if color == 'clear' and levelType == LevelType.INVALID:
					levelType = LevelType.ROTATING
					anchorY = int(gridArray[i+1])
					anchorX = int(gridArray[i])
					if (anchorY&1) == 1:
						anchorX -= 1

				if color == 'starfish':
					levelType = LevelType.WATER

			maxX = max( maxX, int(gridArray[i]) )
			minX = min( minX, int(gridArray[i]) )

			maxY = max( maxY, int(gridArray[i+1]) )
			minY = min( minY, int(gridArray[i+1]) )

		levelType = LevelType.CLEARTOP if levelType == LevelType.INVALID else levelType

		rowLength = maxX - minX + 1

		if levelType == LevelType.CREATURES:
			creatureJson = v[len(creatureSymbol):creatureEnd]
			creatureArray = json.loads( creatureJson )

			borisCount = 0

			outArray.append( len( creatureArray ) )

			for i in range( 0, len( creatureArray ) ):
				if creatureArray[i]["name"] == 'boris':
					borisCount += 1
					levelType = LevelType.BORIS_SINGLE if borisCount == 1 else LevelType.BORIS_MULTI

			for i in range( 0, len( creatureArray ) ):
				outArray.append( creatureArray[i]["name"] )
				outArray.append( creatureArray[i]["name"] + "_" + str(creatureArray[i]["positions"][0]["gridX"]) + "_" + str(creatureArray[i]["positions"][0]["gridY"]) )
				outArray.append( len( creatureArray[i]["positions"] ) )

				if levelType == LevelType.BORIS_SINGLE:
					anchorX = creatureArray[i]["positions"][3]["gridX"]
					anchorY = creatureArray[i]["positions"][3]["gridY"]

				for j in range( 0, len( creatureArray[i]["positions"] ) ):
					x = creatureArray[i]["positions"][j]["gridX"]
					y = creatureArray[i]["positions"][j]["gridY"]

					(q,r) = getQRFromXY( x, y, rowLength )

					outArray.append( int( q ) )
					outArray.append( r )

		outArray.insert( 0, levelType )

		mainfestObj.append( { 'name' : k + ".tdf", 'levelType' : levelType } )

		for i in range( 0, len( gridArray ), 3 ):
			#print( gridArray[i] )
			x = int( gridArray[i] )
			y = int( gridArray[i+1] )
			color = gridArray[i+2]

			(q,r) = getQRFromXY( x, y, rowLength )

			outArray.append( int( q ) );
			outArray.append( r );

			isCreature = color.find( "_" ) != -1;

			if isCreature == True:
				outArray.append( color )
			else:	
				colorCount = colors.count( color );

				if colorCount > 0:
					colorIdx = colors.index( color ) + 1
				else:
					colorIdx = -(specials.index( color ) + 1)

				outArray.append( colorIdx )

		outfile.write( ",".join( str(x) for x in outArray ) )

manifestFile.write( json.dumps( mainfestObj, sort_keys=True, indent=4, separators=(',',': ') ) )
