import sys
import math
import json

rootFolder = "/src/unity-bubble-shot/Assets/StreamingAssets/LevelData/"

manifestFile = open( rootFolder + "LevelManifest.txt", "r" )

manifestText = manifestFile.read()

fileArray = manifestText.split( "\n" )
manifestFile.close()

jsonBlob = []
levelCounter = 0

zoneThresh = len( fileArray ) / 3

for item in fileArray:
	if item == "":
		continue

	levelFile = open( rootFolder + item )
	levelData = levelFile.read()
	levelType = int( levelData[0] )
	levelFile.close()

	starfishCount = 6 if levelType == 0 else 1

	itemDic = {
		'layout' : item,
		'type' : levelType,
		'zone' : int(levelCounter/zoneThresh),
		'moveCount' : 20,
		'star1' : 1000,
		'star2' : 2000,
		'star3' : 3000,
		'starfish' : starfishCount
	}

	jsonBlob.append( itemDic )

	levelCounter = levelCounter + 1

outText = json.dumps( jsonBlob, sort_keys=True,indent=4, separators = (',', ': ') )

print( outText )

outfile = open( "LevelMetadata.txt", "w" )

outfile.write( outText )
outfile.close()