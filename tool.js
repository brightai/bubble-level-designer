xHexes = 11;
yHexes = null;

mainHeight = 2015;

function drawHexGrid(opts,c) {
        
    var alpha       = opts.alpha || 1;
    var color       = opts.color || '#666666';
    var lineWidth   = opts.lineWidth || 1;
    var radius      = opts.radius || 20;
        

    /*
    ctx.beginPath();
    ctx.rect( 0, 0, grid.width, grid.height );
    ctx.stroke();
    */

    r = c.width / xHexes / Math.sqrt(3);

    hexSize = r*Math.sqrt(3);
    yHexSize = r*Math.sqrt(2.25);

    yHexes = yHexes || Math.ceil( mainHeight/ yHexSize );

    var ctx = c.getContext("2d");
    ctx.clearRect(0, 0, c.width, c.height);
    //ctx.globalAlpha = alpha;
    ctx.strokeStyle = color;
    ctx.lineWidth = lineWidth;

    part = 60;

    
    ctx.beginPath();
    
    for( yGrid=0; yGrid < yHexes; yGrid++ )
    {
      var gridW = xHexes - (yGrid&1);

      for( xGrid=0; xGrid < gridW; xGrid++ )
      {
           shiftX = hexSize/2 * ( (yGrid&1) + 1 );

           for( i=0; i<7; i++ )
           {
              var a = i * part - 90;
              x = r * Math.cos(a * Math.PI / 180)+ xGrid*hexSize + shiftX;
              y = r * Math.sin(a * Math.PI / 180)+ yGrid*yHexSize + r;

              if( i == 0 )
              {
                  ctx.moveTo( x,y );
              }
              else
              {
                  ctx.lineTo( x,y );
              }
            }
        }
    }
    ctx.closePath();
    ctx.stroke();

    ctx.beginPath();

    for( var yGrid = yHexes-1; yGrid >= 0; yGrid -= 9 )
    {
        var yPos = (yHexes-1-yGrid) * yHexSize + r/4;

        ctx.strokeStyle = "white";

        var numDashes = 80;
        for( var i = 0; i < numDashes-1; i++ )
        {
          ctx.moveTo( i*c.width/numDashes, yPos);
        
          if( (i&1) == 0 )
          {
              ctx.lineTo( (i+1)*c.width/numDashes, yPos );
          }
        }
    }

    ctx.stroke();
        
    return c;
}

function redrawGridAt( x, y )
{
    var bgcolor = document.body.style.backgroundColor;

    var pos = GetGridPosition( x, y );

    var ctx = grid.getContext("2d");
    ctx.beginPath();
    ctx.fillStyle = bgcolor;
    ctx.strokeStyle = '#666666';
    ctx.arc( pos.x, pos.y , 72/2 * grid.width/800, 0, Math.PI * 2, false );
    ctx.fill();
    ctx.closePath();

    ctx.beginPath();

    shiftX = hexSize/2 * ( (pos.gridY&1) + 1 );

   for( i=0; i<7; i++ )
   {
      var a = i * part - 90;
      var x2 = r * Math.cos(a * Math.PI / 180)+pos.gridX*hexSize + shiftX;
      var y2 = r * Math.sin(a * Math.PI / 180)+pos.gridY*yHexSize + r;

      if( i == 0 )
      {
           ctx.moveTo( x2, y2 );
      }
      else
      {
          ctx.lineTo( x2, y2 );
      }
    }

    ctx.stroke();
}

function redraw()
{
    var ctx = grid.getContext('2d');
    ctx.clearRect(0, 0, grid.width, grid.height);
    ctx.drawImage( canvasTarget, 0, 0 );

    for( var x = 0; x < gridArray.length; x++ )
    {
        for( var y = 0; y < gridArray[x].length; y++ )
        {
            var offset = hexSize/2 * ( (y&1) + 1 );
            if( gridArray[x][y] != null )
            {
                drawBubble_CANVAS( gridArray[x][y], x * hexSize + offset, y * yHexSize + r, 70 *grid.width/800 );
            }

            if( fishGridArray[x][y] != null )
            {
                var xpos = x * hexSize + offset;
                var ypos = y * yHexSize + r;

                var creature = creatureStore[ fishGridArray[x][y] ];

                if( creature == null )
                {
                    drawBubble_CANVAS( fishGridArray[x][y], xpos, ypos, 70 *grid.width/800 );
                }
            }
        }
    }

    for( var i in creatureStore )
    {
        redrawCreature( creatureStore[i] );
    }

    if( canvasDragObject )
    {
        var size = canvasDragObject.width;

        drawBubble_CANVAS( canvasDragObject.type, canvasPos.x, canvasPos.y, size, size );
    }
}

function redrawCreature( creature )
{
    if( creature.name == "save1" )
    {
        var pos = creature.positions[0];

        drawBubble_CANVAS( creature.name, pos.x, pos.y, 70 * grid.width/800 * 1 );        
    }

    if( creature.name == "save3" )
    {
        var pos1 = creature.positions[0];
        var pos2 = creature.positions[1];
        var pos3 = creature.positions[2];

        var midX = (pos1.x + pos2.x + pos3.x) / 3;
        var midY = (pos1.y + pos2.y + pos3.y) / 3;

        drawBubble_CANVAS( creature.name, midX, midY, 70 * grid.width/800 * 2 );
    }

    if( creature.name == "save7" || creature.name == "boris" )
    {
        var pos4 = creature.positions[3];

        drawBubble_CANVAS( creature.name, pos4.x, pos4.y, 70 * grid.width/800 * 3 );
    }
}

function makeEmptyGrid()
{
  var gridArray = new Array( xHexes );
  for( var i = 0; i < yHexes; i++ )
  {
      gridArray[i] = new Array( yHexes );
  }

  return gridArray;
}

function resetGrids()
{
    gridArray = makeEmptyGrid();
    fishGridArray = makeEmptyGrid();

    creatureStore = {}; 
    allCreatures = {};
}

function GetGridPosition( x, y )
{
    var gridY = Math.floor( y / yHexSize );

    if( gridY < 0 || gridY >= yHexes )
    {
        return;
    }

    var offset = hexSize/2 * ( (gridY&1) + 1 );
    var gridX = Math.floor( (x-offset) / hexSize + 0.5 );

    var rowLength = xHexes - (gridY&1);

    if( gridX < 0 || gridX >= rowLength )
    {
        return null;
    }

    var retX = gridX * hexSize + offset;
    var retY = gridY * yHexSize + r;

    return { x:retX, y:retY, gridX:gridX, gridY:gridY };
}

function GetPixelPosition( gridX, gridY )
{
    var offset = hexSize/2 * ( (gridY&1) + 1 );

    var retX = gridX * hexSize + offset;
    var retY = gridY * yHexSize + r;

    return { x:retX, y:retY, gridX:gridX, gridY:gridY };
}

function bubbleDragStart( event )
{
  if( event.dataTransfer != null )
  {
      event.dataTransfer.setData( "Text", event.target.type );

      event.dataTransfer.setData( 'startOffsetX', event.offsetX );
      event.dataTransfer.setData( 'startOffsetY', event.offsetY );
      event.dataTransfer.setData( 'srcSize', event.srcElement.width );
  }
  else
  {
      canvasDragObject = event.target;
  }
}

function addImage( name, isDraggable, container )
{
  var image = new Image();

  var size = 50;

  var filename = imageLookup[name];

  image.src = "images/" + filename + ".png";
  image.width = size;
  image.height = size;

  image.type = name;

  image.draggable = isDraggable || false;

  image.ondragstart = bubbleDragStart;

  container.appendChild( image );
}

function drawBubble_CANVAS( name, x, y, size )
{
    var image = new Image();
    image.type = name;
    image.src = "images/" + imageLookup[name] + ".png";

    image.width = size;
    image.height = size;
    
    var ctx = grid.getContext("2d");
    ctx.drawImage( image, x-size/2, y-size/2, size, size );

    if( specialNames.indexOf(name) > -1 )
    {
        ctx.beginPath();
        ctx.arc( x, y, size/2 - 2, 0, 2*Math.PI, false );
        ctx.closePath();
        ctx.strokeStyle = "white";
        ctx.lineWidth = 2;
        ctx.stroke();
    }

    return image;
}

function addBubbleToGrid( x, y, name )
{
    if( overlays.indexOf( name ) > -1 )
    {
        addOverlayToGrid( x, y, name );
        lastColor = name;
        return;
    }

    if( creatures.indexOf( name ) > -1 )
    {
        addCreatureToGrid( x, y, name );
        lastColor = name;
        return;
    }

    var pos = GetGridPosition( x, y );

    if( pos == null )
    {
        return;
    }

    if( gridArray[pos.gridX][pos.gridY] == name )
    {
        return;
    }

    lastColor = name;
    drawBubble_CANVAS( lastColor , pos.x, pos.y, 70 *grid.width/800 );

    gridArray[pos.gridX][pos.gridY] = lastColor;

    cacheGrid();
}

function addOverlayToGrid( x, y, name )
{
    console.log("adding overlay");
    var pos = GetGridPosition( x, y );

    if( pos == null )
    {
        return;
    }

    drawBubble_CANVAS( name , pos.x, pos.y, 70 *grid.width/800 );

    fishGridArray[pos.gridX][pos.gridY] = name;

    cacheGrid();
}

function addCreatureToGrid( x, y, name )
{
    var offset = hexSize/2;
    
    var xoffset = hexSize/2;
    var yoffset = yHexSize/2;

    var positions = new Array();

    if( name == "save1" )
    {
        positions.push( GetGridPosition( x, y ) );
    }

    if( name == "save3" )
    {
        positions.push( GetGridPosition( x, y-offset ) );
        positions.push( GetGridPosition( x-offset, y+offset ) );
        positions.push( GetGridPosition( x+offset, y+offset ) );
    }

    if( name == "save7" || name == "boris" )
    {
        positions.push( GetGridPosition( x-xoffset, y-yoffset*2 ) );
        positions.push( GetGridPosition( x+xoffset, y-yoffset*2 ) );

        positions.push( GetGridPosition( x-xoffset*2, y ) );
        positions.push( GetGridPosition( x, y ) );
        positions.push( GetGridPosition( x+xoffset*2, y ) );

        positions.push( GetGridPosition( x-xoffset, y+yoffset*2 ) );
        positions.push( GetGridPosition( x+xoffset, y+yoffset*2 ) );
    }

    var creature = { name:name, positions:positions };
    var isValid = true;

    for( var i = 0; i < positions.length; i++ )
    {
        var pos = positions[i];

        if( fishGridArray[pos.gridX][pos.gridY] )
        {
            isValid = false;
            break;
        }
    }

    if( !isValid )
    {
        return;
    }

    var creatureHash = getCreatureHash( creature );
    creatureStore[creatureHash] = creature;
    allCreatures[creatureHash] = creature;

    for( var i = 0; i < positions.length; i++ )
    {
        var pos = positions[i];

        fishGridArray[pos.gridX][pos.gridY] = creatureHash;
    }

    cacheGrid();
}

function getCreatureHash( creature )
{
    return creature.name + "_" + creature.positions[0].gridX + "_" + creature.positions[0].gridY;
}

function removeObjectFromGrid( pos )
{
    if( fishGridArray[pos.gridX][pos.gridY] )
    {
        if( creatureStore[ fishGridArray[pos.gridX][pos.gridY] ] != null )
        {
            var creatureHash = fishGridArray[pos.gridX][pos.gridY];
            var creature = creatureStore[creatureHash];

            for( var j in creature.positions )
            {
                var creaturePos = creature.positions[j];
                fishGridArray[creaturePos.gridX][creaturePos.gridY] = null;
            }

            delete creatureStore[creatureHash];
        }
        else
        {
            fishGridArray[pos.gridX][pos.gridY] = null;
        }
    }
    else
    {
        gridArray[pos.gridX][pos.gridY] = null;
    }
}

function cacheGrid()
{
    cacheIndex++;

    gridArrayCache.splice( cacheIndex, 0, JSON.stringify( gridArray ) );
    fishGridArrayCache.splice( cacheIndex, 0, JSON.stringify( fishGridArray ) );

    gridArrayCache.splice( cacheIndex+1, gridArrayCache.length-cacheIndex );

    if( gridArrayCache.length == 11 )
    {
        gridArrayCache.shift();
        fishGridArrayCache.shift();

        cacheIndex--;
    }
}

function clearCache()
{
    gridArrayCache = [];
    fishGridArrayCache = [];
    cacheIndex = -1;
}

function undo()
{
    if( gridArrayCache.length > 0 && cacheIndex > 0 )
    {
        cacheIndex--;

        gridArray = JSON.parse( gridArrayCache[cacheIndex] );
        fishGridArray = JSON.parse( fishGridArrayCache[cacheIndex] );

        updateCreatureStore();
        redraw();
    }
}

function redo()
{
    if( gridArrayCache.length > 0 && cacheIndex < (gridArrayCache.length-1) )
    {
        cacheIndex++;

        gridArray = JSON.parse( gridArrayCache[cacheIndex] );
        fishGridArray = JSON.parse( fishGridArrayCache[cacheIndex] );

        updateCreatureStore();
        redraw();
    }
}

function updateCreatureStore()
{
    var existingCreatures = {};

    for( var x = 0; x < gridArray.length; x++ )
    {
        for( var y = 0; y < gridArray[x].length; y++ )
        {
            if( fishGridArray[x][y] != null )
            {
                var creatureHash = fishGridArray[x][y];
                var creature = creatureStore[creatureHash];

                if( creature )
                {
                    existingCreatures[creatureHash] = creature;
                }
                else if( allCreatures[creatureHash] )
                {
                    existingCreatures[creatureHash] = allCreatures[creatureHash];
                }
            }
        }
    }

    creatureStore = existingCreatures;
}

function cancel( event )
{
    if( event.preventDefault )
    {
        event.preventDefault();
    }

    return false;
}

function drop( event )
{
    if( event.preventDefault )
    {
        event.preventDefault();
    }

    if( event.dataTransfer != null )
    {
        var type = event.dataTransfer.getData('Text');

        var startOffsetX = event.dataTransfer.getData( 'startOffsetX' );
        var startOffsetY = event.dataTransfer.getData( 'startOffsetY' );
        var srcSize = event.dataTransfer.getData( 'srcSize' );

        addBubbleToGrid( event.offsetX - startOffsetX + srcSize/2, event.offsetY - startOffsetY + srcSize/2, type );
        redraw();
    }

    return false;
}

function click( event )
{
    if( canvasDrag && event.button < 2 )
    {
        console.log( "click" );
        canvasDrag = false;
        canvasDragObject = null;
        return;
    }

    if( event.button < 2 )
    {
        if( canvasDragObject )
        {
            addBubbleToGrid( event.offsetX, event.offsetY, canvasDragObject.type );
            canvasDragObject = null;
            redraw();
        }
        else
        {
            addBubbleToGrid( event.offsetX, event.offsetY, lastColor );
            redraw();
        }
    }
    else
    {
        cacheGrid();

        var pos = GetGridPosition( event.offsetX, event.offsetY );

        removeObjectFromGrid( pos );

        //redrawGridAt( event.offsetX, event.offsetY );
        redraw();
    }
}

function mouseDown( event )
{
    canvasDrag = true;
    canvasPos.x = event.offsetX;
    canvasPos.y = event.offsetY;
}

function mouseMove( event )
{
    var sqDist = Math.pow( event.offsetX - canvasPos.x, 2 ) + Math.pow( event.offsetY - canvasPos.y, 2 );
    if( sqDist < (4*4) )
    {
        return;
    }

    var size = 50;

    if( event.button < 2 && canvasDrag )
    {
        var pos = GetGridPosition( canvasPos.x, canvasPos.y );
        canvasPos.x = event.offsetX;
        canvasPos.y = event.offsetY;

        var obj = fishGridArray[pos.gridX][pos.gridY] || gridArray[pos.gridX][pos.gridY];
        var type;

        if( canvasDragObject == null && obj != null )
        {
            if( creatureStore[obj] != null )
            {
                var creature = creatureStore[obj];
                type = creature.name;

                var clusterSize = creature.positions.length;
                size = size * (1 + Math.sqrt( 1 - 4 * (1-clusterSize) )) / 2;
            }
            else
            {
                type = obj;
            }

            canvasDragObject = drawBubble_CANVAS( type, event.offsetX, event.offsetY, size );

            removeObjectFromGrid( pos );
        }
        
        if( canvasDragObject )
        {
            redraw();
        }
    }
}

function mouseUp( event )
{
   canvasDrag = false;
}

function onclickButton( el )
{
    if( el.id == "saveButton" )
    {
        saveGrid();
    }
    else if( el.id == "loadButton" )
    {
        if( fileSelector.length > 0 )
        {
            loadGrid( fileSelector.value );
        }
    }
    else if( el.id == "resetButton" )
    {
        resetGrids();
        redraw();
    }
    else if( el.id == "undoButton" )
    {
        undo();
    }
    else if( el.id == "redoButton" )
    {
        redo();
    }
    else if( el.id == "newButton" )
    {
        var newName = prompt( "Please enter a name for the new file", "New Grid" );

        if( newName == null )
        {
           alert( "You must supply a name!" );
        }
        else
        {
            if( fileSelector.options[newName] != null )
            {
               alert( "A file with that name already exists!" );
               return;
            }

            resetGrids();
            redraw();

            addOption( newName );
            fileSelector.value = newName;
            var renameBox = document.getElementById('renameBox');
            renameBox.value = newName;
        }
    }
    else if( el.id == "destroyButton" )
    {
        var result = confirm( "Destroy all -- are you sure??" );

        if( result )
        {
            FileSystem.clearAll();
            loadFileData();
        }
    }
    else if( el.id == "exportButton" )
    {
        createDownloadLink();
    }
    else if( el.id == "importButton" )
    {
        var fileInput = document.getElementById('fileChooser');
        fileInput.click();
    }
    else if( el.id == "commitButton" )
    {
        var sortList = document.getElementById('levelList');
        var items = sortList.getElementsByTagName('li');

        var fileList = [];

        for( var i = 0; i < items.length; i++ )
        {
            fileList.push( items[i].innerHTML );
        }

        FileSystem.setBlob( "fileList", fileList );

        loadFileData();
    }
}

function handleFile( f )
{
    var reader = new FileReader();
    reader.onload = function( e )
    {
        var fileInput = document.getElementById('fileChooser');
        fileInput.value = null;

        var importArray = JSON.parse( reader.result );

        FileSystem.clearAll();

        var fileList = [];

        for( var i = 0; i < importArray.length; i++ )
        {
            var dic = importArray[i];
            for( var key in dic )
            {
                FileSystem.setItem( key, dic[key] );
                fileList.push( key );
            }
        }

        FileSystem.setBlob( "fileList", fileList );

        loadFileData();
    }

    reader.onerror = function( e )
    {
        alert( "Failed to load " + f.name );
    }

    reader.readAsText( f );
}

function importDragOver( e )
{
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy';
}

function importDrop( e )
{
    e.stopPropagation();
    e.preventDefault();

    handleFile( e.dataTransfer.files[0] );
}

function onFileSelectChanged()
{
    var filename = fileSelector.value;
    var renameBox = document.getElementById('renameBox');
    renameBox.value = filename;

    loadGrid( filename );
}

function onChangeName( el )
{
    var newName = el.value;

    for( var i =0; i < fileSelector.length; i++ )
    {
        if( fileSelector.options[i].value == newName )
        {
            alert( "A file with that name already exists!! " );
            el.value = fileSelector.value;
            return;
        }
    }

    var oldName = fileSelector.value;

    var idx = fileSelector.selectedIndex;
    fileSelector.options[idx] = new Option( newName, newName );
    fileSelector.value = newName;

    saveGrid();

    FileSystem.removeItem( oldName );
}

function createDownloadLink()
{
    var exportArray = [];

    for( var i=0; i < fileSelector.length; i++ )
    {
        var name = fileSelector.options[i].value;
        var data = loadSerialisedGrid( name );
        var gridDic = {};
        gridDic[name] = data;

        exportArray.push( gridDic );
    }

    var exportString = JSON.stringify( exportArray );

    var a = document.createElement( "a" );
    a.download = "export.json";
    a.title = "Download Export Data";
    a.href = "data:text/json;charset=utf-8," + encodeURIComponent( exportString );
    a.innerHTML = "download data";

    a.click();
}

function serialiseGrid()
{
    var dataArray = new Array();

    var creatureStoreSize = Object.keys( creatureStore ).length;
    var creatureArrayString = "[";

    if( creatureStoreSize > 0 )
    {
        for( var k in creatureStore )
        {
            creatureArrayString += JSON.stringify( creatureStore[k] ) + ",";
        }

        creatureArrayString = creatureArrayString.substring( 0, creatureArrayString.length - 1 );
        creatureArrayString += "]";

        dataArray.push( creatureSymbol + creatureArrayString + "#" );
    }

    for( var y = 0; y < yHexes; y++ )
    {
        var rowLength = xHexes - (y&1);

        for( var x = 0; x < rowLength; x++ )
        {
            if( gridArray[x][y] != null )
            {
                var bubbleData = x + ";" + y + ";" + gridArray[x][y];
                dataArray.push( bubbleData );
            }

            if( fishGridArray[x][y] )
            {
                var bubbleData = x + ";" + y + ";" + fishGridArray[x][y];
                dataArray.push( bubbleData );   
            }
        }
    }

    return dataArray;
}

function saveGrid()
{
    var dataArray = serialiseGrid();

    if( dataArray.length > 0 )
    {
        var filename;
        if( fileSelector.length > 0 )
        {
            filename = fileSelector.value;
        }
        else
        {
            filename = "gridData_0";

            var option = document.createElement( "option" );
            option.text = filename;

            fileSelector.add( option );
        }

        if( FileSystem.setItem( filename, dataArray.join( ";" ) ) )
        {
            var fileList = [].map.call( fileSelector.options, function(el){ return el.value; } );
            if( FileSystem.setBlob( "fileList", fileList ) )
            {
                loadFileData();
                alert( filename + " : successfully saved!" );
            }
        }
    }
}

function loadSerialisedGrid( name )
{
    var gridData = FileSystem.getItem( name );

    if( gridData == null )
    {
       console.log( "no data!" );
       return;
    }

    return gridData;
}

function loadGrid( name )
{
    var gridData = loadSerialisedGrid( name );

    var creatureStart = gridData.indexOf( creatureSymbol );
    var loadedCreatureStore = {};

    if( creatureStart === 0 )
    {
        creatureStart = creatureSymbol.length;
        var creatureEnd = gridData.indexOf( '#', creatureStart );

        var creatureJson = gridData.substring( creatureStart, creatureEnd );
        var creatureArray = JSON.parse( creatureJson );

        for( var i = 0; i < creatureArray.length; i++ )
        {
            var creatureHash = getCreatureHash( creatureArray[i] );
            loadedCreatureStore[creatureHash] = creatureArray[i];
        }

        gridData = gridData.substring( creatureEnd +2 );
    }

    gridData = gridData.split( ";" );

    resetGrids();

    creatureStore = loadedCreatureStore;

    for( var i = 0; i < gridData.length; i+= 3 )
    {
        var x = gridData[i];
        var y = gridData[i+1];
        var color = gridData[i+2];

        var rowLength = xHexes - (y&1);

        if( x < 0 || x >= rowLength )
        {
            continue;
        }

        if( creatureStore[color] != null ||  overlays.indexOf( color ) > -1 )
        {
            fishGridArray[x][y] = color;
        }
        else
        {
            gridArray[x][y] = color;
        }
    }

    redraw();

    FileSystem.setItem( "lastLoaded", name );

    clearCache();
    cacheGrid();
}

function addOption( optionName )
{
    if( optionName == "" )
    {
       return false;
    }

    var option = document.createElement( "option" );
    option.text = optionName;
    fileSelector.add( option );

    return true;
}

function loadFileData()
{
    fileSelector = document.getElementById('fileSelect');

    while( fileSelector.options.length )
    {
        fileSelector.remove( 0 );
    }

    var sortList = document.getElementById('levelList');

    sortList.innerHTML = "";

    var fileList = FileSystem.getBlob( "fileList" );

    if( fileList )
    {
        for( var i = 0; i < fileList.length; i++ )
        {
            var filename = fileList[i];
            var option = document.createElement( "option" );
            option.text = filename;
            fileSelector.add( option );

            var listItem = document.createElement( 'li' );
            listItem.appendChild( document.createTextNode( filename ) );
            sortList.appendChild( listItem );
        }

        $('.sortable').sortable();

        var lastLoaded = FileSystem.getItem( "lastLoaded" );
        loadGrid( lastLoaded || fileSelector.value );

        fileSelector.value = lastLoaded || fileSelector.value;
    }

    var renameBox = document.getElementById('renameBox');
    renameBox.value = fileSelector.value;
}

function preloadImages()
{
    var loadCount = 0;
    for( var key in imageLookup )
    {
        var image = new Image();

        image.onload = function()
        {
            loadCount--;
            if( loadCount == 0 )
            {
                preloadImages_Finished();
            }
        };

        var filename = imageLookup[key];
        image.type = name;
        image.src = "images/" + filename + ".png";

        loadCount++;
    }
}

window.oncontextmenu = function() { return false; };

var container = document.getElementById('grid');

var scrollDiv = document.createElement( 'div' );
scrollDiv.className = 'scrollbar-measure';
container.appendChild( scrollDiv );

var scrollBarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;

container.removeChild( scrollDiv );

container.style.width = 400 + scrollBarWidth;

var grid = document.createElement("canvas");
grid.width  = 400;
grid.height = 800;

var canvasTarget = document.createElement("canvas");
canvasTarget.width  = 400;
canvasTarget.height = mainHeight;

drawHexGrid({}, canvasTarget);

container.appendChild( grid );

var newCanvasHeight = yHexes * yHexSize - r;
grid.height = newCanvasHeight;

grid.addEventListener( 'dragover', cancel );
grid.addEventListener( 'dragenter', cancel );
grid.addEventListener( 'drop', drop );
grid.addEventListener( 'click', click );

grid.oncontextmenu = click;

var canvasDrag = false;
var canvasDragObject = null;
var canvasPos = { x: 0, y: 0 };

grid.addEventListener( 'mousedown', mouseDown );
grid.addEventListener( 'mousemove', mouseMove );
grid.addEventListener( 'mouseup', mouseUp );

var importButton = document.getElementById('importButton');
importButton.addEventListener( 'dragover', importDragOver, false );
importButton.addEventListener( 'drop', importDrop, false );

var bubbleNames = [
  "blue",
  "red",
  "yellow",
  "purple",
  "green"
];

var specialNames = [
  "puffer",
  "clearer",
  "extra",
  "anchor",
  "ink",
  "blocker",
  "inkblocker",
  "flounder",
  "stone",
  "whirl",
];

var overlays = [
  "starfish",
  "blocker",
  "ink",
  "inkblocker",
];

var creatures = [
    "save1",
    "save3",
    "save7",

    "boris"
];

var imageLookup = {
  "puffer" : "helper_bomb",
  "clearer" : "helper_line_clear",
  "extra" : "helper_extra_moves",
  "anchor" : "helper_enchanted_coral",
  "ink" : "hinder_ink_highlight",
  "blocker" : "appbar.timer.2",
  "inkblocker" : "hinder_ink_blocker",
  "flounder" : "hinder_flounder",
  "stone" : "hinder_stone",
  "whirl" : "hinder_whirlpool",

  "blue" : "bubble_blue",
  "red" : "bubble_red",
  "yellow" : "bubble_yellow",
  "purple" : "bubble_purple",
  "green" : "bubble_green",

  "clear" : "bubble_clear",

  "starfish" : "starfish",

  "boris" : "boris_bubble_placeholder",

  "save1" : "save_character_1x",
  "save3" : "save_character_3x",
  "save7" : "save_character_7x",
}

var creatureSymbol = "CREATURES#";

var lastColor = "blue";

var gridArray = makeEmptyGrid();
var fishGridArray = makeEmptyGrid();
var creatureStore = {};
var allCreatures = {};

var gridArrayCache = [];
var fishGridArrayCache = [];

var cacheIndex = -1;

for( i = 0; i < bubbleNames.length; i++ )
{
    addImage( bubbleNames[i], true, bubbles );
}

for( i = 0; i < specialNames.length; i++ )
{
    addImage( specialNames[i], true, specials );
}

preloadImages();

function preloadImages_Finished()
{
  loadFileData();

  redraw();
  container.scrollTop = container.scrollHeight;
}

