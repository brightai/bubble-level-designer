FileSystem = {};

FileSystem.setItem = function( name, value )
{
	try
	{
		localStorage.setItem( name, value );
	}
	catch( e )
	{
		if( e == QUOTA_EXCEEDED_ERR )
		{
			alert( "Quota exceeded!" );
		}

		return false;
	}

	return true;
}

FileSystem.getItem = function( name )
{
	if( typeof( localStorage ) == 'undefined' )
	{
		alert( "Your browser does not support HTML5 Storage! :(" );
	}
	else
	{
		return localStorage.getItem( name );
	}
}

FileSystem.removeItem = function( name )
{
	if( typeof( localStorage ) == 'undefined' )
	{
		alert( "Your browser does not support HTML5 Storage! :(" );
	}
	else
	{
		localStorage.removeItem( name );
	}
}

FileSystem.setBlob = function()
{
	var name;
	var items = "";

	name = arguments[0];

	if( arguments.length == 2 && typeof( arguments[1] ) == 'object' && Array.isArray( arguments[1] ) )
	{
		return this.setItem( name, arguments[1].join(';') );
	}

	for( var i = 1; i < arguments.length; i++ )
	{
		items += arguments[i];

		if( i < arguments.length-1 )
		{
			items += ";";
		}
	}

	return this.setItem( name, items );
}

FileSystem.getBlob = function( name )
{
	var items = this.getItem( name );
	
	if( !items )
	{
		return;
	}

	items = items.split( ";" );

	return items;
}

FileSystem.clearAll = function()
{
	localStorage.clear();
}
